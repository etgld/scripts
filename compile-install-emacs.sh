#!/bin/bash
JOBS=`nproc --ignore=2`

cd ~/Downloads
sudo apt update -y
sudo apt upgrade -y
sudo apt install -y build-essential \
     texinfo libx11-dev libxpm-dev \
     libjpeg-dev libpng-dev libgif-dev \
     libtiff-dev libncurses-dev \
     libgnutls28-dev libgtk-3-dev \
     libtree-sitter-dev \
     libmagick++-dev libxml2-dev libtool \
     autoconf  libgccjit-14-dev  libjansson-dev mailutils

export CC=/usr/bin/gcc CXX=/usr/bin/g++

# wget https://ftp.gnu.org/gnu/emacs/emacs-29.4.tar.xz
# tar xaf emacs-29.4.tar.xz

# cd emacs-29.4
cd ~/Repos/emacs
# Options:
#    --with-native-compilation  ->  use the libgccjit native compiler
#    --with-pgtk                ->  better font rendering
#    --with-x-toolkit=gtk3      ->  widgets toolkit
#    --with-tree-sitter         ->  syntax parsing
#    --with-wide-int            ->  larger file size limit
#    --with-json                ->  fast JSON
#    --with-gnutls              ->  TLS/SSL
#    --with-mailutils           ->  e-mail
#    --without-pop              ->  no pop3 (insecure channels)
#    --with-cairo               ->  vector graphics backend
#    --with-imagemagick         ->  raster images backend
./autogen.sh \
./configure --with-native-compilation \
    --with-pgtk \
    --with-x-toolkit=gtk3 \
    --with-tree-sitter \
    --with-wide-int \
    --with-json \
    --with-modules \
    --without-dbus \
    --with-gnutls \
    --with-mailutils \
    --without-pop \
    --with-cairo \
    --with-imagemagick \
    # Other interesting compilation options:
    #
    #--prefix=""                    # output binaries location
    #--with-x-toolkit=lucid         # supposedly more stable
    #--with-xwidgets
    # Compiler flags:
    # -O2                   ->  Turn on a bunch of optimization flags. There's also -O3, but it increases
    #                           the instruction cache footprint, which may end up reducing performance.
    # -pipe                 ->  Reduce temporary files to the minimum.
    # -mtune=native         ->  Optimize code for the local machine (under ISA constraints).
    # -march=native         ->  Enable all instruction subsets supported by the local machine.
    # -fomit-frame-pointer  ->  Small functions don't need a frame pointer (optimization).
    #
    # https://lemire.me/blog/2018/07/25/it-is-more-complicated-than-i-thought-mtune-march-in-gcc/
    CFLAGS="-O2 -pipe -mtune=native -march=native -fomit-frame-pointer"
make -j${JOBS} NATIVE_FULL_AOT=1
sudo make install
