#!/bin/bash
# Works on Ubuntu 24, for Ubuntu 23 change boost
# versions from 1.83 to 1.81
sudo add-apt-repository ppa:ichthyo/zeug
# Notionally you can install electric sheep and flam3
# from the above Ubuntu repo but I haven't been able
# to get it working
cd ~/Repos

git clone git@github.com:scottdraves/flam3.git
cd flam3/
libtoolize
aclocal
automake --add-missing
autoconf
autoupdate
./configure
make
sudo make install

cd ../
git clone git@github.com:scottdraves/electricsheep.git
cd electricsheep/client_generic/
autoreconf
./autogen.sh
sudo apt install ffmpeg libavcodec-dev \
     libavformat-dev libswscale-dev \
     libboost-dev libx11-dev \
     liblua5.1-dev libpng-dev \
     mesa-common-dev libxrender-dev \
     libexpat-dev libjpeg-dev \
     libmotif-dev libfreetype6-dev \
     glee-dev libcurl4-gnutls-dev \
     libgtop2-dev libboost-thread1.83-dev \
     libboost-filesystem1.83-dev libtinyxml-dev \
     libglut-dev libwxgtk-media3.2-dev
./configure
make
sudo make install
